// [SECTION] Dependecies and Modules
const express = require("express");
const productController = require("../controllers/productController")
const auth = require("../auth")
const {verify, verifyAdmin} = auth;

const router = express.Router();

const multer = require("multer")
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '../frontend/src/pictures/addProduct')
    },
    filename: function (req, file, cb) {
    const uniqueSuffix = file.originalname
    cb(null, file.fieldname + '-' + uniqueSuffix)
    }
});
const upload = multer({storage});

// ----------------------------------------------------------------------
router.post("/", upload.single('image'), verify, verifyAdmin, productController.addProduct)

router.get("/allProducts", productController.getAllProducts)

router.get("/allActiveProducts", productController.getAllActive)

router.get("/:productId", productController.getProduct)

router.put("/:productId", verify, verifyAdmin, productController.updateProduct)

router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct)

router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct)

router.post('/searchByName', productController.searchProductsByName);	






module.exports = router









